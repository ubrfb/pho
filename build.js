
const _  = require('underscore'),
      debug = require('debug')('build'),
      crypto = require('crypto'),
      im = require('imagemagick'),
      fs = require('fs');

const inputDir = './input';
const outputDir = './output';

const resolutions = [ '640x960', '640x1136', '720x1280', '2048x1536' ];

var files = fs.readdirSync(inputDir);

function md5(text) {
	var sum = crypto.createHash('md5');
	sum.update(text);
	return sum.digest('hex');
}

_.each(files, function(file) {
	outputId = md5(file);

	debug('processing ' + file + ' -> ' + outputId);

	_.each(resolutions, function(res) {
		var outputFile = outputId + '-' + res + '.jpg';
		im.convert([ inputDir + '/' + file, '-resize', res + '^', '-gravity', 'center', '-extent', res, outputDir + '/' + outputFile ]);
	});
});

